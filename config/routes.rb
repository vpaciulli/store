Rails.application.routes.draw do

  # match '/' => redirect('http://store.kiik.com.br'), via: [:get, :post]
  # match '*path' => redirect('http://store.kiik.com.br'), via: [:get, :post]

  # Root Route
  root to: 'pages#home'

  # Admin
  mount Shoppe::Engine => '/admin'

  # Browse Products
  get 'products' => 'products#categories', :as => 'catalogue'
  get 'products/filter' => 'products#filter', :as => 'product_filter'
  get 'products/:category_id' => 'products#index', :as => 'products'
  get 'products/:category_id/:product_id' => 'products#show', :as => 'product'
  post 'products/:category_id/:product_id/buy' => 'products#add_to_basket', :as => 'buy_product'

  # Order Status
  get 'order/:token' => 'orders#status', as: 'order_status'

  # Basket
  get 'basket' => 'orders#wizard', :as => 'basket'
  delete 'basket' => 'orders#destroy', :as => 'empty_basket'
  post 'basket/:order_item_id' => 'orders#change_item_quantity', :as => 'adjust_basket_item_quantity'
  delete 'basket/delete/:order_item_id' => 'orders#remove_item', :as => 'remove_basket_item'
  delete 'basket/delete/ajax/:order_item_id' => 'orders#remove_item_ajax', :as => 'remove_basket_item_ajax'

  # Checkout
  match 'checkout' => 'orders#checkout', :as => 'checkout', :via => [:get, :patch]
  match 'checkout/refresh' => 'orders#update_order', :as => 'update_order', :via => [:get, :patch]
  match 'checkout/pay' => 'orders#payment', :as => 'payment', :via => [:get, :patch]
  match 'checkout/pay-now-kiik', to: 'orders#pay_now', as: 'pay_now', via: [:get, :patch]
  match 'checkout/one-click-buy', to: 'orders#one_click_buy', as: 'one-click-buy', via: [:post, :patch]

  match 'checkout/billet' => 'orders#billet', :as => 'billet', :via => [:get, :patch]
  match 'checkout/pay-pal' => 'orders#paypal', :as => 'paypal', :via => [:get, :patch]
  match 'checkout/card-information' => 'orders#card_information', :as => 'card_information', :via => [:get, :patch]
  match 'checkout/redirect-pay-pal' => 'orders#redirect_paypal', :as => 'redirect_paypal', :via => [:get, :patch]

  match 'checkout/finalize' => 'orders#finalize', :as => 'finalize', via: :get
  match 'checkout/finish-order/:id' => 'orders#finish_order', :as => 'finish_order', via: :put

end
