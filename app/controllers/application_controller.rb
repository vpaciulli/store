class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  skip_before_filter :verify_authenticity_token

  private

  # Returns active order for the session
  def current_order
  	@current_order ||= begin
  		if has_order?
  			@current_order
  		else
  			order = Shoppe::Order.create(ip_address: request.ip)
  			session[:order_id] = order.id
  			order
  		end
  	end
  end

  def has_order?
    !!(session[:order_id] &&
       @current_order = Shoppe::Order.includes(order_items: :ordered_item).find_by_id(session[:order_id]))
  end

  def authenticate
     #Check cookies
    #if Rails.env == 'development'|| Rails.env == 'production'
      #cookies[:mlg_id] = { value: 2249, expires: 5.days.from_now }
      #cookies[:mlg_login] = { value: 'julienlucca', expires: 5.days.from_now }
      #cookies['account-token'] = { value: '22b1f120-5f94-0132-f419-02034aacd59a', expires: 5.minutes.from_now }
    #end

    #if cookies[:mlg_id]
      #true
    #else
      #false
    #end
  end

  helper_method :current_order, :has_order?

end
