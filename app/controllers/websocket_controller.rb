class WebsocketController < WebsocketRails::BaseController
  def initialize_session
    # perform application setup here
    controller_store[:message_count] = 0
  end

  def user_connected
    p 'user connected'
    p session[:session_id]
    WebsocketRails.users[client_id] = connection
    $redis.set(session[:session_id], client_id)
    $redis.set(session[:kiik_token], client_id) if session[:kiik_token]
  end
end
