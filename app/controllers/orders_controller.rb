# coding: utf-8
require 'httparty'
include ActionView::Helpers::NumberHelper

class OrdersController < ApplicationController
  include ActiveMerchant::Billing
  include PaypalExpressHelper
  include WebsocketRails

  before_action :authenticate

  def show
    @hide_header = true
  end

  def wizard
    @hide_header = true
    unless current_order.order_items.any?
      flash[:notice] = 'Carrinho vazio! (:'
      redirect_to root_path
    end
    payment
  end

  def checkout
    @order = current_order
    @hide_header = true
    if request.patch? #&& @order.proceed_to_confirm(order_params)
      redirect_to checkout_payment_path
    end
  end

  def payment
    @code = create_order(current_order)
  end

  def create_order(order)
    return if order.order_items.empty?
    @order = Shoppe::Order.includes(order_items: :ordered_item).find_by_id(order.id)
    items = @order.order_items.map do |item|
      {
        product_description: item.ordered_item.name,
        price_unit: item.ordered_item.price.to_s,
        quant: item.quantity
      }
    end

    header = {
      Accept: 'application/json'
    }

    auth = {
      username: '8210882a5815600efc6090455e3b1178228cebd7',
      password: ''
    }

    body = {
      order: {
        metadata: {
          callback_url: "http://#{request.headers['HTTP_HOST']}/checkout/finish-order",
          items: items
        },
        amount: (items.map { |x| x[:price_unit].to_f * x[:quant]}.sum * 100).to_i,
        company_identifier: "010477",
        order_type: 'physical'
      }
    }
    url = 'https://admin.kiik.com/api/v2/orders'

    response = HTTParty.post(url, header: header, body: body, basic_auth: auth)

    unless response.success?
      flash[:alert] = 'uh oh, houve um erro com a comunicação com o Kiik ):'
      return redirect_to checkout_path
    end
    @order.update(kiik_token: response["order"]["code"])
    session[:kiik_token] = @order.kiik_token
    $redis.set(@order.kiik_token, $redis.get(session[:session_id]))
    response["order"]["code"]
  end

  def billet
    @order = current_order
  end

  def paypal
    @hide_header = true
  end

  def card_information
    @hide_header = true
    @order = current_order
  end

  def redirect_paypal
    @order = current_order

    items = @order.order_items.map do |item|
      {
        name: item.ordered_item.name,
        description: item.ordered_item.description,
        quantity: item.quantity,
        amount: to_cent(item.ordered_item.price),
      }
    end

    response = EXPRESS_GATEWAY.setup_purchase(to_cent(@order.total),
      items: items,
      ip: request.remote_ip,
      return_url: confirmation_url,
      cancel_return_url: catalogue_url,
      brand_name: 'Kiik::Store',
      cart_border_color: '000CD')
    unless response.success?
      flash[:alert] = 'uh oh, houve um erro com a comunicação com o Paypal ):'
      return redirect_to checkout_path
    end

    redirect_to EXPRESS_GATEWAY.redirect_url_for(response.token)
  end

  def finalize
    @order = Shoppe::Order.includes(order_items: :ordered_item).find_by_id(session[:order_id])
    @hide_header = false
    session[:order_id] = nil
    session[:kik_token] = nil
    current_order = nil
    render partial: 'orders/finalize', layout: false
  end

  def finish_order
    @order = Shoppe::Order.includes(order_items: :ordered_item).find_by_kiik_token(params[:id])
    Shoppe::Country.new(name: 'Brasil').save! unless Shoppe::Country.first

    @payment = OpenStruct.new(params[:order])
    @payment.user = OpenStruct.new(params[:order][:user])
    @payment.address = OpenStruct.new(params[:order][:addresses].first)

    @order.update(
      first_name: @payment.user.name.partition(' ').first,
      last_name: @payment.user.name.partition(' ').last,
      billing_address1: "#{@payment.address.street}, #{@payment.address.number}",
      billing_address2: @payment.address.neighborhood,
      billing_address3: @payment.address.city,
      billing_address4: @payment.address.state,
      billing_postcode: @payment.address.zip,
      billing_country: Shoppe::Country.first,
      email_address: @payment.user.email,
      phone_number: @payment.user.phone,
      status: 'shipped',
      received_at: Time.now,
      accepted_at: Time.now,
      shipped_at: Time.now,
      amount_paid: @payment.amount * 0.01
    )

    Shoppe::Payment.new(
      order_id: @order.id,
      amount: @payment.amount * 0.01,
      reference: rand(0..100000),
      method: 'KiiK'
    ).save

    @payment.amount = number_to_currency(@payment.amount * 0.01, unit: 'R$', separator: ",", delimiter: ".")

    resume_view = render(partial: 'orders/resume', layout: false, formats: [:html])
    WebsocketRails.users[$redis.get(params[:id])].send_message :order_finished, resume_view

    render html: "ok".html_safe
  end

  def remove_item
    current_order_item = Shoppe::OrderItem.find(params[:order_item_id])
    current_order_item.destroy

    return redirect_to basket_path if current_order.order_items.any?
    redirect_to root_path
  end

  def remove_item_ajax
    current_order_item = Shoppe::OrderItem.find(params[:order_item_id])
    current_order_item.destroy
    @order = Shoppe::Order.includes(order_items: :ordered_item).find_by_id(current_order.id)
    @code = create_order(@order)
    respond_to do |format|
      format.json { return render(partial: 'orders/pay_now', layout: false, formats: [:html])}
    end
  end

  def destroy
    @current_order.destroy
    session[:order_id] = nil
    redirect_to root_path, notice: 'Basket Empty (:'
  end

  def change_item_quantity
    order_item_id = params['order_item_id']
    quantity = params['quantity']
    @order = Shoppe::Order.includes(order_items: :ordered_item).find_by_id(current_order.id)
    order_item = @order.order_items.find(order_item_id)
    order_item.quantity = quantity
    order_item.save!
    order_item = @order.order_items.find(order_item_id)
    item_total = number_to_currency(order_item.ordered_item.price * order_item.quantity, unit: "R$", separator: ",", delimiter: ".")
    total = number_to_currency(order_item.order.total_before_tax, unit: "R$", separator: ",", delimiter: ".")
    @code = create_order(@order)
    @qrcode = RQRCode::QRCode.new(@code)
    qr= ''
    @qrcode.modules.each_index do |x|
      qr+= '<tr>'
      @qrcode.modules.each_index do |y|
        if @qrcode.dark?(x,y)
          qr += '<td class="black"></td>'
        else
          qr += '<td class="white"></td>'
        end
      end
      qr+= '</tr>'
    end
    render json: {item_total: item_total, total: total, code: @code, qr: qr}
  end

  def update_order
  end

  def pay_now
    @order = Shoppe::Order.includes(order_items: :ordered_item).find_by_id(current_order.id)
    @code = create_order(@order)
    respond_to do |format|
      format.json { return render(partial: 'orders/pay_now', layout: false, formats: [:html])}
    end
  end

  def one_click_buy
    @product = Shoppe::Product.find_by_permalink!(params['permalink'])
    @order = Shoppe::Order.includes(order_items: :ordered_item).find_by_id(current_order.id)
    @order.order_items.add_item(@product, 1)
    @updated_order = Shoppe::Order.includes(order_items: :ordered_item).find_by_id(current_order.id)
    @code = create_order(@updated_order)
    respond_to do |format|
      format.json { return render(partial: 'orders/pay_now', layout: false, formats: [:html])}
    end
  end

  private

  def order_params
    params[:order].permit(
      :first_name,
      :last_name,
      :billing_address1,
      :billing_address2,
      :billing_address3,
      :billing_address4,
      :billing_country_id,
      :billing_postcode,
      :email_address,
      :phone_number
    )
  end

  def create_payment(order)
    544515
  end
end
