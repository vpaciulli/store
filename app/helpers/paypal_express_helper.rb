module PaypalExpressHelper
  def get_setup_purchase_params(credit, request)
    subtotal, tax, total = get_totals(credit)
    return total, {
      ip: request.remote_ip,
      return_url: url_for(action: 'review', only_path: false),
      cancel_return_url: url_for(action: 'error', only_path: false),
      subtotal: subtotal,
      shipping: 0,
      handling: 0,
      tax: tax,
      allow_note: true,
      items: [{
        name: "Credits Package (#{credit.amount})",
        number: "CP_#{credit.amount}",
        quantity: 1,
        amount: total
      }]
    }
  end

  def get_order_info(gateway_response, credit)
    subtotal, tax, total = get_totals(credit)
    {
      shipping_address: gateway_response.address,
      email: gateway_response.email,
      name: gateway_response.name,
      gateway_details: {
        :token => gateway_response.token,
        :payer_id => gateway_response.payer_id,
      },
      subtotal: subtotal,
      tax: tax,
      total: total,
    }
  end

  def get_purchase_params(credit, request)
    subtotal, tax, total = get_totals(credit)
    return total, {
      ip: request.remote_ip,
      token: credit.token,
      payer_id: credit.payer_id,
      subtotal: subtotal,
      shipping: 0,
      handling: 0,
      tax: tax,
      items: [{
        name: "Credits Package (#{credit.amount})",
        number: "CP_#{credit.amount}",
        quantity: 1,
        amount: total
      }]
    }
  end

  def to_cent(value)
    (value*100).round
  end

  def get_totals(credit)
    subtotal = to_cent(credit.subtotal)
    tax = to_cent(credit.tax)
    total = subtotal + tax
    return subtotal, tax, total
  end
end
